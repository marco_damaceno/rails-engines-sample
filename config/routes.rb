Rails.application.routes.draw do
  namespace :admin do
    mount Registration::Engine => '/', as: 'registration'
  end
end
