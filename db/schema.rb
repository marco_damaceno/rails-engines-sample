# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151008032106) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "adminpack"

  create_table "registration_users", force: :cascade do |t|
    t.string   "crypted_password"
    t.string   "salt"
    t.string   "email",            null: false
    t.string   "firstname",        null: false
    t.string   "lastname",         null: false
    t.string   "birthdate"
    t.integer  "doc"
    t.integer  "kind"
    t.integer  "gender"
    t.integer  "telephone"
    t.integer  "cellphone"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "registration_users", ["email"], name: "index_registration_users_on_email", unique: true, using: :btree

end
