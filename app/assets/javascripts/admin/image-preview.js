function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      var blah = $('.blah');
      blah.attr('src', e.target.result).attr('height', '100');
      $('#cropper').attr('src', e.target.result);
      $('#clear').css('display', 'inline');
      // console.log(blah.attr('src'));
    };
    reader.readAsDataURL(input.files[0]);
  }
}

$("#user_profile_image").change(function() {
  readURL(this);
});

var control = $('#user_profile_image');

$("#clear").on("click", function () {
    control.replaceWith( control = control.clone( true ) );
    $('.blah').removeAttr('src').attr('height', '0');
    $('#clear').css('display', 'none');
});
