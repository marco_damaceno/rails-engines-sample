//= require admin/jquery.min.js
//= require admin/jquery-ui.min.js

//= require admin/plugins/charts/sparkline.min.js
//= require admin/plugins/forms/uniform.min.js
//= require admin/plugins/forms/select2.min.js
//= require admin/plugins/forms/inputmask.js
//= require admin/plugins/forms/autosize.js
//= require admin/plugins/forms/inputlimit.min.js
//= require admin/plugins/forms/listbox.js
//= require admin/plugins/forms/multiselect.js
//= require admin/plugins/forms/validate.min.js
//= require admin/plugins/forms/tags.min.js
//= require admin/plugins/forms/switch.min.js

//= require admin/plugins/forms/uploader/plupload.full.min.js
//= require admin/plugins/forms/uploader/plupload.queue.min.js
//= require admin/plugins/forms/wysihtml5/wysihtml5.min.js
//= require admin/plugins/forms/wysihtml5/toolbar.js

//= require admin/plugins/interface/daterangepicker.js
//= require admin/plugins/interface/fancybox.min.js
//= require admin/plugins/interface/moment.js
//= require admin/plugins/interface/jgrowl.min.js
//= require admin/plugins/interface/datatables.min.js
//= require admin/plugins/interface/colorpicker.js
//= require admin/plugins/interface/fullcalendar.min.js
//= require admin/plugins/interface/timepicker.min.js

//= require bootstrap/dist/js/bootstrap.min.js
//= require admin/application-londinium.js

//= require jquery-ujs/src/rails.js

//= require admin/image-preview.js
//= require cropit/dist/jquery.cropit.js
//= require jquery-mask-plugin/src/jquery.mask.js
//= require jquery-maskmoney/dist/jquery.maskMoney.min.js
//= require admin/masks.js
